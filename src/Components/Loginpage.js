import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import axios from'axios';


function Loginpage(args) {
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  let [email,setEmail] = useState("");
  let [password,setPassword] = useState("");
 
  const handleSubmit = async (e) =>{
    e.preventDefault();
    // alert(email + " " + password)
    try{
      let response = await axios.post("http://localhost:3003/login",{"email":email, "password":password})
      console.log(response)
      alert(response.data)

    }catch(err){
      console.log(err);
    }
    
  }

  return (
    <div>
      <Button color="danger" onClick={toggle}>
        Login
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Enter your Details</ModalHeader>
        <ModalBody>
        <Form onSubmit={handleSubmit}>
  <FormGroup>
    <Label
      for="exampleEmail"
      hidden
    >
      Email
    </Label>
    <Input
      id="exampleEmail"
      name="email"
      placeholder="Email"
      type="email"
      value={email}
      onChange={(e)=>{setEmail(e.target.value);}}
    />
  </FormGroup>
  {' '}
  <FormGroup>
    <Label
      for="examplePassword"
      hidden
    >
      Password
    </Label>
    <Input
      id="examplePassword"
      name="password"
      placeholder="Password"
      type="password" 
      required
      value ={password}
      onChange={(e)=>{setPassword(e.target.value);}}
    />
  </FormGroup>
  {' '}
  <Button type = "submit">
    Submit
  </Button>
</Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Loginpage;