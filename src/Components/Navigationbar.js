import React from 'react'
import { Link } from 'react-router-dom';
import logo from "./images/logo.webp";
import Loginpage from "./Loginpage";

function Navigationbar() {
    const navbardesign = {
        backgroundColor:'lightgrey', 
        color:'azure',
        display :'flex', 
        justifyContent: 'space-between',
        // border : '3px solid black',
        height : '80px',
    }
    const linkdesign = {
        display :  'flex',
        // border : '2px solid red',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        width : '50%',
        height : '60%',
        marginRight: '10%',


    }
    

  return (
    <div>
        <div style={navbardesign}>
        <img src={logo} style = {{width:"350px", height:"80px"}}/>
        <div style = {linkdesign}>
            <Link to = "/" style={{ textDecoration: 'none',color:"darkgreen"}}>Home</Link>
            {/* <Link to = "/Login" style={{ textDecoration: 'none',color:"darkgreen"}}>Login</Link> */}
            <Link to = "/ContactUs" style={{ textDecoration: 'none',color:"darkgreen" }}>Contact Us</Link>
            <Link to = "/AboutUs" style={{ textDecoration: 'none',color:" darkgreen" }}>About Us</Link>
            <Loginpage/>
        </div>
        
        
        </div>

    </div>
  )
}

export default Navigationbar