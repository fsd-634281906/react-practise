import './App.css';
import {BrowserRouter,Route,Routes,Link} from 'react-router-dom';
import Home from './Components/Home';
import Login from './Components/Login';
import ContactUs from './Components/ContactUs';
import AboutUs from './Components/AboutUs';
import Navigationbar from './Components/Navigationbar';
import Cards1 from './Components/Cards1';


function App() {
  return (
    <div className='app'>
      <BrowserRouter>
      <Navigationbar/>
      <Routes>
        <Route path ="/" element = {<Home/>}/>
        {/* <Route path ="/Login" element = {<Login/>}/> */}
        <Route path = "/ContactUs" element = {<ContactUs/>}/>
        <Route path = "/AboutUs" element = {<AboutUs/>}/>
      </Routes>
      </BrowserRouter>

      



    </div>


  );
}

export default App;
